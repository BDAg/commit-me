const Commit = require('../models/Commit');

module.exports = {
  async create(request, response) {
    const { content } = request.body;

    try{
      const commit = await Commit.create({
        content
      });
      
      response.status(201).send({message: "Criado com Sucesso"});

    } catch(err){
      response.status(400).send(err);
    }

  },

  async list(request, response) {
    try {
      const commit = await Commit.find();
      response.status(200).send(commit);

    } catch(err){
      response.status(204).send({mensagem: err});
    }
  },

  async listId(request, response) {
    try{
      const commit = await Commit.findById(request.params.id);
      response.status(200).send(commit);

    }catch(err) {
      response.status(204).send({mensage: err});
    }
  },

  async update(request,response) {
    try {
      const commit = await Commit.findByIdAndUpdate(request.params.id, request.body, {new: true});
      response.status(200).json(commit);
      
    }catch(err) {
      response.status(400).send({mensage: err});
    }
  },

  async delete(request, response) {
    try{
      const commit = await Commit.findOneAndDelete(request.params.id);
      response.status(200).send()
    }catch(err){
      response.status(400).send({message: err});
    }
  }
}
