const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const authConfig = require('../config/auth.json');
const { API_KEY } = require('../config/mail.json');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(API_KEY);

function generateToken(params = {}) {
    return jwt.sign(params, authConfig.secret, {
        expiresIn: 86400,
    });
};

module.exports = {
    async list(request, response) {
        const users = await User.find();

        return response.json(users);
    },

    async register(request, response) {
        const { email, username, password } = request.body;

        let user = await User.findOne({ email });

        if (!user) {
            user = await User.create({
                email,
                username,
                password,
            });

            user.password = undefined;

            return response.json({
                user,
                token: generateToken({ id: user.id }),
            });
        };

        return response.status(400).send({ error: 'Email is already in use' });
    },

    async login(request, response) {
        const { email, password } = request.body;
    
        const user = await User.findOne({ email }).select('+password');
    
        if (!user) {
            return response.status(400).send({ error: 'Invalid email' });
        };
    
        if (!await bcrypt.compare(password, user.password)) {
            return response.status(400).send({ error: 'Invalid password' });
        };
    
        user.password = undefined;
    
        response.json({
            user,
            token: generateToken({ id: user.id }),
        });
    },
    
    async home(request, response) {
        response.send({ ok: true, user: request.userId });
    },

    async forgot(request, response) {
        const { email } = request.body;
    
        const user = await User.findOne({ email });
    
        if (!user) {
            return response.status(400).send({ error: 'User not found' });
        };
    
        const token = generateToken({ id: user.id });
    
        const now = new Date();
        now.setHours(now.getHours() + 1);
    
        await User.findByIdAndUpdate(user.id, {
            '$set': {
                passwordResetToken: token,
                passwordResetExpires: now,
            }
        }, { new: true, useFindAndModify: false });
    
        const link = `http://localhost:3333/reset_password/${token}`;
    
        const msg = {
            to: email,
            from: 'andrew8gmf@gmail.com',
            subject: 'Change your CommitMe account password',
            html: `<p>Esqueceu sua senha? Clique aqui: ${link}</p>`,
        };
    
        try {
            await sgMail.send(msg);
            return response.send({ message: 'Password reset link has been successfully sent to your inbox' });
        } catch (error) {
            console.error(error);
    
            if (error.response) {
                console.error(error.response.body)
            }
        }
    },
    
    async reset(request, response) {
        const { password } = request.body;
        const { token } = request.params;
        const decoded = jwt.verify(token, authConfig.secret);
    
        const user = await User.findOne({ _id: decoded.id }).select('+passwordResetToken passwordResetExpires');
    
        if (!user) {
            return response.status(400).send({ error: 'User not found' });
        };
    
        if (token !== user.passwordResetToken) {
            return response.status(400).send({ error: 'Token invalid' });
        };
    
        const now = new Date();
    
        if (now > user.passwordResetExpires) {
            return response.status(400).send({ error: 'Token expired, generate a new one' });
        };
    
        user.password = password;
    
        await user.save();
    
        response.send({ message: 'Password changed' });
    },

};