const mongoose = require("mongoose");

const CommitSchema = new mongoose.Schema({
  content: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model("Commit", CommitSchema);
