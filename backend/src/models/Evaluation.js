const mongoose = require("mongoose");

const EvaluationSchema = new mongoose.Schema({
  rated: {
    type: mongoose.Types.ObjectId,
    ref: "Profile",
    required: true
  },
  evaluator: {
    type: mongoose.Types.ObjectId,
    ref: "Profile",
    required: true
  },
  punctuation: {
    type: mongoose.Types.Decimal128,
    required: true
  },
  evaluationDate: {
    type: Date,
    default: Date.now,
    required: true
  }
});

module.exports = mongoose.model("Evaluation", EvaluationSchema);
