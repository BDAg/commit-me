const mongoose = require("mongoose");

const ProfileSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  profile_image: {
    type: String
  },
  bio: {
    type: String
  },
  userId: {
    type: mongoose.Types.ObjectId,
    ref: "User",
    required: true
  },
  commitId: {
    type: mongoose.Types.ObjectId,
    ref: "Commit"
  },
  contactId: [{
    type: mongoose.Types.ObjectId,
    ref: "Profile"
  }],
  skills: {
    type: [{type: String}]
  },
  evaluation: {
    type: [{
      type: mongoose.Types.ObjectId, 
      ref: "Evaluation"
    }]
  },
  punctuation: {
    type: mongoose.Types.Decimal128
  }
});

module.exports = mongoose.model("Profile", ProfileSchema);
