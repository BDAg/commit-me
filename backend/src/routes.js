const { Router } = require('express');

const authMiddleware = require('./middlewares/auth');

const UserController = require('./controllers/UserController');
const CommitController = require('./controllers/CommitController');

const routes = Router();

routes.get('/users', UserController.list);

routes.post('/register', UserController.register);
routes.post('/login', UserController.login);
routes.get('/home', authMiddleware, UserController.home);

routes.post('/forgot_password', UserController.forgot);
routes.post('/reset_password/:token', UserController.reset);

routes.get('/commits', CommitController.list);
routes.get('/commit/:id', CommitController.listId);
routes.post('/post_commit', CommitController.create);
routes.put('/put_commit/:id', CommitController.update);
routes.delete('/delete_commit/:id', CommitController.delete);

module.exports = routes;