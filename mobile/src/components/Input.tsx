import React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
const { width : WIDTH } = Dimensions.get('window');

interface InputProps{
  placeholderText: string
}

const Input:React.FC<InputProps> = ({placeholderText}) =>{
  return(
    <View>
      <TextInput
          style={styles.input}
          placeholder={placeholderText}
          placeholderTextColor={'rgba(255, 255, 255, 0.7)'}
          underlineColorAndroid='transparent'
      />
    </View>
  )
}

export default Input;

const styles = StyleSheet.create({
  input:{
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(0, 0, 0, 0.35)',
    color: 'rgba(255, 255, 255, 0.7)',
    marginHorizontal: 25,
    marginVertical: 20,
  },
})