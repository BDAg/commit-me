import React, { ReactNode, useState } from 'react';
import { View, StyleSheet, Dimensions, StatusBar } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Feather } from '@expo/vector-icons';
import { BorderlessButton } from 'react-native-gesture-handler';
import Input from '../Input';

interface PageHeaderProps {
    headerSearch?: ReactNode
}



const PageHeader: React.FC<PageHeaderProps> = ({ headerSearch, children }) => {
    // const [isFiltersVisible, setIsFiltersVisible] = useState(false);
    const { navigate } = useNavigation();

    function handleNavigationToProfile() {
        navigate('Profile');
    }

    function handleNavigationToHome() {
        navigate('Home');
    }

    // function handleToggleFiltersVisible() {
    //     setIsFiltersVisible(!isFiltersVisible);
    // }

    // setIsFiltersVisible(false);


    return (

        <View style={styles.container}>
            <StatusBar barStyle="light-content" backgroundColor="#000000" />
            <View style={styles.buttonNav}>
                <BorderlessButton
                    style={styles.buttonHome}
                    onPress={handleNavigationToHome}
                >
                    <Feather name="home" size={45} color="black" />
                </BorderlessButton>

                <BorderlessButton
                    style={styles.buttonSearch}
                    // onPress={handleToggleFiltersVisible}
                >
                    <Feather name="search" size={45} color="black" />
                </BorderlessButton>


                <BorderlessButton
                    style={styles.buttonProfile}
                    onPress={handleNavigationToProfile}
                >
                    <Feather name="user" size={45} color="black" />
                </BorderlessButton>
            </View>
            {/* {isFiltersVisible && (

                    <Input placeholderText="Pesquisar" />
                


            )} */}
            {/* {children} */}
        </View>
    )
}

export default PageHeader;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 110,
        backgroundColor: '#c4c4c4',
        justifyContent: 'center',

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,

        elevation: 12,
    },
    buttonNav: {
        marginTop: 30,
        flexDirection: "row",
        justifyContent: "space-around",

    },
    buttonHome: {
        height: 60,
        width: 60,
        borderRadius: 100,
        backgroundColor: '#70707030',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonSearch: {
        height: 60,
        width: 60,
        borderRadius: 100,
        backgroundColor: '#70707030',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonProfile: {
        height: 60,
        width: 60,
        borderRadius: 100,
        backgroundColor: '#70707030',
        justifyContent: 'center',
        alignItems: 'center'
    }
})