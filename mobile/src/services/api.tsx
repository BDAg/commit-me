import axios from "axios";

const api = axios.create({
  baseURL: "https://api-commit-me.herokuapp.com"
});

export default api;