import React, { Profiler } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const {Navigator, Screen} = createStackNavigator();

import SingIn from './pages/SingIn';
import SingUp from './pages/SingUp';
import ForgetPassword from './pages/ForgetPassword';
import Profile from './pages/Profile';
import Home from './pages/Home';
import Network from './pages/Network';
import PageHeader from './components/PageHeader';

export default function Routes(){
  return(
    <NavigationContainer>
      <Navigator screenOptions={{headerShown: false}}>
        <Screen
          name="SingIn" 
          component={SingIn}
        />
        <Screen 
          name="SingUp" 
          component={SingUp}
        />
        <Screen 
          name="ForgetPassword" 
          component={ForgetPassword}
        />
        <Screen 
          name="Home" 
          component={Home}
          options={{
            headerShown: true,
            header: ()  => <PageHeader/>
          }}
        />
        <Screen 
          name="Profile" 
          component={Profile}
          options={{
            headerShown: true,
            header: ()  => <PageHeader/>
          }}
        />
        <Screen 
          name="Network" 
          component={Network}
        />
      </Navigator>
    </NavigationContainer>
  )
}