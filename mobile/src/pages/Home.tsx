import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { BorderlessButton, ScrollView, TextInput } from 'react-native-gesture-handler';
import { Feather } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
const { width : WIDTH } = Dimensions.get('window');

import api from '../services/api';

function Home({ navigation }) {

    const { navigate } = useNavigation();

    const [users, setUsers] = useState([]);
    const [search, setSearch] = useState("");
    const [filteredUsers, setFilteredUsers] = useState([]);

    async function fetchData() {
        await api.get('/users').then(response => {
            setUsers(response.data);
        });
    }

    useEffect(() => {
        fetchData();
    });

    useEffect(() => {
        setFilteredUsers(
          users.filter((user) =>
            user.username.toLowerCase().includes(search.toLowerCase())
          )
        );
    }, [search, users]);


    return (
        <>
            <ScrollView>
                <View style={styles.container}>
                    <TextInput style={styles.searchInput}
                    placeholder="Search usernames here"
                    value={search}
                    onChangeText={setSearch}></TextInput>
                    {filteredUsers.map(user => (
                        <BorderlessButton
                            onPress={() => {
                                navigation.navigate('Profile', { 
                                    username: user.username
                                });
                            }}
                        >
                            <View style={styles.cardContaner}>
                                <View style={styles.infoContainer}>
                                    <Feather name="user" size={45} color="black" />
                                    <Text key={user._id} style={styles.username}> {user.username} </Text>   
                                </View>
                                <Text style={styles.description}>Programador Back-end</Text>
                            </View>

                        </BorderlessButton>
                    ))}
                </View>
            </ScrollView>

        </>
    )
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 10,
        flex: 1,
        alignItems: 'center'
    },
    searchInput: {
        width: WIDTH,
        height: 45,
        borderRadius: 25,
        fontSize: 16,
        paddingLeft: 45,
        backgroundColor: 'rgba(0, 0, 0, 0.35)',
        color: 'rgba(255, 255, 255, 0.7)',
    },
    cardContaner:{
        marginVertical: 20,
        alignItems: 'center',
        justifyContent: 'center',
        width: 330,
        height: 130,
        backgroundColor: '#CDDCDC',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,

        elevation: 12,
    },
    infoContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        
        justifyContent: 'center',
        
    },
    username: {
        fontSize: 28,
        fontWeight: '600'
    },
    description: {
        fontSize: 22
         
    },

});

export default Home;