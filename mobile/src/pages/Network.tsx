import React, { useState } from 'react';
import { View, Text, StyleSheet, } from 'react-native';
import { BorderlessButton, RectButton, ScrollView } from 'react-native-gesture-handler';
import { Feather } from '@expo/vector-icons';

import { useNavigation } from '@react-navigation/native';

function Network() {

    const {navigate} = useNavigation();

    function goToBack(){
        navigate('Profile')
    }

    return (
        <>
            <View style={styles.headerNetwork}>
                <BorderlessButton
                    style={styles.arrowToBack}
                    onPress={goToBack}
                >
                    <Feather name="arrow-left" size={45} color="black" />
                </BorderlessButton>
                <View style={styles.textContainer}>
                    <Text style={styles.textHeader}>Minha Rede</Text>
                </View>

            </View>
            <ScrollView>
                <View style={styles.container}>
                    <BorderlessButton>
                        <View style={styles.cardContaner}>
                            <View style={styles.infoContainer}>
                                <Feather name="user" size={45} color="black" />
                                <Text style={styles.username}> Joselino Rousseff </Text>   
                            </View>
                            <Text style={styles.description}>Programador Back-end</Text>
                        </View>
                    </BorderlessButton>
                </View>
            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 10,
        marginTop: 40,
        flex: 1,
        alignItems: 'center'
    },
    headerNetwork: {
        backgroundColor: '#7b7b7b',
        width: '100%',
        height: 110,
        
        alignItems: 'center',

        flexDirection: "row",

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,

        elevation: 12,
    },
    arrowToBack: {
        height: 60,
        width: 60,
        borderRadius: 100,
        backgroundColor: '#70707030',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textHeader: {
        fontSize: 32,
        fontWeight: "700",
    },
    textContainer:{
        height: 100,
        marginLeft: '17%',
        position: "relative",
        alignSelf: 'center',
        justifyContent: 'center',
        
    },
    cardContaner:{
        marginVertical: 20,
        alignItems: 'center',
        justifyContent: 'center',
        width: 330,
        height: 130,
        backgroundColor: '#CDDCDC',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,

        elevation: 12,
    },
    infoContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        
        justifyContent: 'center',
        
    },
    username: {
        fontSize: 28,
        fontWeight: '600'
    },
    description: {
        fontSize: 22
         
    },
});

export default Network;