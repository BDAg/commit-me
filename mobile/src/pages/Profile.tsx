import { useRoute } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { View, Text, ImageBackground, Image, StyleSheet, Dimensions} from 'react-native';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
const { width : WIDTH } = Dimensions.get('window');

const userImage = require('../assets/images/user.jpeg');
const bgImage = require('../assets/images/bg2.jpg');

function Profile({ route, navigation }){
  const { username } = route.params;
  return(
    <ScrollView>
      <View style={styles.content}>
        <ImageBackground source={bgImage} style={styles.bgImage}>
          <Image source={userImage} style={styles.profileImage}/>
        </ImageBackground>
        <View style={styles.profileInfo}>
          <Text style={styles.userTitle}>
            { username }
          </Text>
          <Text style={styles.userBio}>
            Eu acordei, tenho roupas para vestir, tenho água para beber, tenho comida para comer.
            A vida é boa, eu sou grato. Amo viver e gosto de programar.
          </Text>
          <Text style={styles.userTitle}>
            Habilidades
          </Text>
          <View style={styles.userSkillsBox}>
            <RectButton style={styles.userSkillButton}>
              <Text style={styles.userSkillText}>
                Node.js
              </Text>
            </RectButton>
            <RectButton style={styles.userSkillButton}>
              <Text style={styles.userSkillText}>
                Node.js
              </Text>
            </RectButton>
            <RectButton style={styles.userSkillButton}>
              <Text style={styles.userSkillText}>
                Node.js
              </Text>
            </RectButton>
            <RectButton style={styles.userSkillButton}>
              <Text style={styles.userSkillText}>
                Node.js
              </Text>
            </RectButton>
            <RectButton style={styles.userSkillButton}>
              <Text style={styles.userSkillText}>
                Node.js
              </Text>
            </RectButton>
            <RectButton style={styles.userSkillButton}>
              <Text style={styles.userSkillText}>
                Node.js
              </Text>
            </RectButton>
            <RectButton style={styles.userSkillButton}>
              <Text style={styles.userSkillText}>
                Node.js
              </Text>
            </RectButton>
            <RectButton style={styles.userSkillButton}>
              <Text style={styles.userSkillText}>
                Node.js
              </Text>
            </RectButton>
            <RectButton style={styles.userSkillButton}>
              <Text style={styles.userSkillText}>
                Node.js
              </Text>
            </RectButton>
          </View>
        </View>
        <View style={styles.navigateButtons}>
          <RectButton 
            style={styles.button}
          >
            <Text style={styles.navigateButtonText}>
              COMMITS
            </Text>
          </RectButton>

          <RectButton style={styles.button}
            onPress={() => {
              navigation.navigate('Network');
            }}>
            <Text style={styles.navigateButtonText}>
              AMIGOS
            </Text>
          </RectButton>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  content:{
    flex: 1,
  },
  bgImage:{
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  profileImage:{
    borderColor: '#000',
    borderWidth: 2,
    width: 150,
    height: 150,
    borderRadius: 100,
    marginTop: 180,
  },
  profileInfo:{
    marginTop: 80,
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignContent: 'center',
  },
  userTitle:{
    width: WIDTH -40,

    textAlign: 'center',
    fontSize: 30,

    color: '#fff',
    backgroundColor: '#2f2f2f',
  },
  userBio:{
    marginVertical: 20,
    padding: 10,
    width: WIDTH -40,

    textAlign: 'justify',
    fontSize: 15,

    borderColor: '#000',
    borderWidth: 2,
  },
  userSkillsBox:{
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: 10,
    width: WIDTH -20,
    height: 150,

    alignItems: 'center',
    justifyContent: 'center',
  },
  userSkillButton:{
    width: 105,
    height: 45,
    margin: 1,

    alignItems: 'center',
    justifyContent: 'center',

    backgroundColor: '#234',
  },
  userSkillText:{
    textAlign: 'center',
    color: '#fff'
  },
  navigateButtons:{
    alignItems: 'center',
    justifyContent: 'center',
  },
  button:{
    marginVertical: 10,
    width: WIDTH -40,
    height: 40,

    alignItems: 'center',
    justifyContent: 'center',

    backgroundColor: '#2f2f2f',
  },
  navigateButtonText:{
    fontSize: 25,
    color: '#fff',
  },
});

export default Profile;