import React from 'react';
import {View, Text, TextInput, ImageBackground, Image, StyleSheet, Dimensions} from 'react-native';
import {RectButton, TouchableOpacity} from 'react-native-gesture-handler';
const { width : WIDTH } = Dimensions.get('window');

const bgLight = require('../assets/images/bg1.jpg')
const logoA = require('../assets/images/logoA.png')

import { useNavigation } from '@react-navigation/native';
import Input from '../components/Input';

function ForgetPassword(){
  const {goBack} = useNavigation();

  function handleGoBack(){
    goBack();
  }

  return(
    <ImageBackground source={bgLight} style={styles.Backgroundcontainer}>
      <View style={styles.logoContainer}>
        <Image source={logoA} style={styles.logo}/>
        <Text style={styles.logoText}>commit_me</Text>
      </View>
      <View>
        <Input placeholderText="Email"/>
      </View>
      <View>
        <RectButton style={styles.button}>
          <Text style={styles.buttonText}>
            Enviar
          </Text>
        </RectButton>
        <TouchableOpacity onPress={handleGoBack}>
          <Text style={styles.registerText}>Voltar</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  Backgroundcontainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  logoContainer:{
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    marginBottom: 130,
  },

  logo:{
    width: 100,
    height: 100
  },
  logoText:{
    marginLeft: -10,
    fontSize: 30,
    fontWeight: 'bold',
  },
  button:{
    marginTop: 100,
    width: WIDTH - 105,
    height: 45,
    borderRadius: 25,
    backgroundColor: 'rgba(250, 250, 250, 0.8)',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  buttonText:{
    fontSize: 26,
    fontWeight: 'bold',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  registerText:{
    marginTop: 10,
    textAlign: 'center',
    fontSize: 16,
    color: '#d9d9d9'
  },
});

export default ForgetPassword;