import React from 'react';
import {View, Text, ImageBackground, Image, TextInput, StyleSheet, Dimensions} from 'react-native';
import {RectButton, TouchableOpacity} from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
const { width : WIDTH } = Dimensions.get('window');

const bgDark = require('../assets/images/bgDark.jpg');
const logoB = require('../assets/images/logoB.png');

import api from '../services/api';

function SingUp(){
  const {navigate} = useNavigation();

  function handleNavigationToSingIn(){
    navigate('SingIn');
  }

  const [email, onChangeEmail] = React.useState('');
  const [username, onChangeUsername] = React.useState('');
  const [password, onChangePassword] = React.useState('');
  const [confirmPassword, onChangeConfirmPassword] = React.useState('');

  async function handleCreateUser() {
    if (!username || !email || !password) {
      alert("Preencha todos os dados para se cadastrar" );
    } else {
      if (password!==confirmPassword){
        alert("Senha de confirmação incorreta" );
      
      }else{
        try {
          await api.post("/register", { username, email, password });
          alert("Cadastro realizado com sucesso!" );
          navigate('SingIn');
        } catch (err) {
          console.log(err);
          alert("Ocorreu um erro ao registrar sua conta");
        }
      }
    }
  }

  return(
    <ImageBackground source={bgDark} style={styles.Backgroundcontainer}>
      <View style={styles.logoContainer}>
        <Image source={logoB} style={styles.logo}/>
        <Text style={styles.logoText}>commit_me</Text>
      </View>
      <View>
      <TextInput
        style={styles.input}
        placeholder={'Nome'}
        placeholderTextColor={'rgba(255, 255, 255, 0.7)'}
        underlineColorAndroid='transparent'
        onChangeText={text => onChangeUsername(text)}
        value={username}
      />
      <TextInput
        style={styles.input}
        placeholder={'Email'}
        placeholderTextColor={'rgba(255, 255, 255, 0.7)'}
        underlineColorAndroid='transparent'
        onChangeText={text => onChangeEmail(text)}
        value={email}
      />
      <TextInput
        style={styles.input}
        placeholder={'Senha'}
        placeholderTextColor={'rgba(255, 255, 255, 0.7)'}
        underlineColorAndroid='transparent'
        secureTextEntry={true} 
        onChangeText={text => onChangePassword(text)}
        value={password}
      />
      <TextInput
        style={styles.input}
        placeholder={'Confirmar senha'}
        placeholderTextColor={'rgba(255, 255, 255, 0.7)'}
        underlineColorAndroid='transparent'
        secureTextEntry={true} 
        onChangeText={text => onChangeConfirmPassword(text)}
        value={confirmPassword}
      />
      </View>
      <View>
        <RectButton style={styles.button} onPress={handleCreateUser}>
          <Text style={styles.buttonText}>
            Cadastrar
          </Text>
        </RectButton>
        <TouchableOpacity onPress={handleNavigationToSingIn}>
          <Text style={styles.aboveButtonText}>Cancelar</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  Backgroundcontainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  logoContainer:{
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    marginBottom: 60,
  },
  logo:{
    width: 100,
    height: 100
  },
  logoText:{
    marginLeft: -10,
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff'
  },
  button:{
    marginTop: 50,
    width: WIDTH - 105,
    height: 45,
    borderRadius: 25,
    backgroundColor: 'rgba(250, 250, 250, 0.8)',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  buttonText:{
    fontSize: 26,
    fontWeight: 'bold',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  aboveButtonText:{
    marginTop: 10,
    textAlign: 'center',
    color: '#fff'
  },
  input:{
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(0, 0, 0, 0.50)',
    color: 'rgba(255, 255, 255, 0.7)',
    marginHorizontal: 25,
    marginVertical: 20,
  },
});

export default SingUp;