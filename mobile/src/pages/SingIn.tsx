
import React from 'react';
import {View, Text, TextInput, ImageBackground, Image, StyleSheet, Dimensions} from 'react-native';
import { RectButton, TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

const { width : WIDTH } = Dimensions.get('window');

const bgLight = require('../assets/images/bg1.jpg')
const logoA = require('../assets/images/logoA.png')

import api from '../services/api';

function SingIn(){

  const {navigate} = useNavigation();

  function handleNavigationToSingUp(){
    navigate('SingUp');
  }

  function handleNavigationToForgetPassword(){
    navigate('ForgetPassword');
  }

  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');

  async function handleLogin() {
    if ( !email || !password ) {
      alert("Digite email e senha para continuar");
    } else {
      try {
        await api.post("/login", { email, password });
        alert("Login efetuado com sucesso" );
        navigate('Home');
      } catch (err) {
        console.log(err);
        alert("Ocorreu um erro ao logar");
      }
    }
  }

  return(
    <ImageBackground source={bgLight} style={styles.Backgroundcontainer}>
      <View style={styles.logoContainer}>
        <Image source={logoA} style={styles.logo}/>
        <Text style={styles.logoText}>commit_me</Text>
      </View>

      <View>
        <TextInput
          style={styles.input}
          placeholder={'Email'}
          placeholderTextColor={'rgba(255, 255, 255, 0.7)'}
          underlineColorAndroid='transparent'
          onChangeText={text => setEmail(text)}
          value={email}
        />
        <TextInput
          style={styles.input}
          placeholder={'Senha'}
          placeholderTextColor={'rgba(255, 255, 255, 0.7)'}
          underlineColorAndroid='transparent'
          secureTextEntry={true} 
          onChangeText={text => setPassword(text)}
          value={password}
        />
      </View>

      <View>
        <RectButton onPress={handleLogin} style={styles.button}>
          <Text style={styles.buttonText}>
            Login
          </Text>
        </RectButton>
        <TouchableOpacity onPress={handleNavigationToSingUp}>
          <Text style={styles.registerText}>Cadastre-se!</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={handleNavigationToForgetPassword}>
          <Text style={styles.registerText}>Esqueci minha senha</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  Backgroundcontainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  logoContainer:{
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    marginBottom: 130,
  },
  logo:{
    width: 100,
    height: 100
  },
  logoText:{
    marginLeft: -10,
    fontSize: 30,
    fontWeight: 'bold',
  },
  button:{
    marginTop: 100,
    width: WIDTH - 105,
    height: 45,
    borderRadius: 25,
    backgroundColor: 'rgba(250, 250, 250, 0.8)',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  buttonText:{
    fontSize: 26,
    fontWeight: 'bold',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  registerText:{
    marginTop: 10,
    textAlign: 'center',
    fontSize: 16,
    color: '#d9d9d9'
  },
  input:{
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(0, 0, 0, 0.50)',
    color: 'rgba(255, 255, 255, 0.7)',
    marginHorizontal: 25,
    marginVertical: 20,
  },
});

export default SingIn;