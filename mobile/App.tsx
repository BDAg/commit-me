import React from 'react';
import { StatusBar } from 'expo-status-bar';
import {StyleSheet, View, Text} from 'react-native';

import Routes from './src/routes';

export default function App() {
  return (
    <>
      <Routes/>
      <StatusBar style="inverted"/>
    </>
  );
}

const styles = StyleSheet.create({
  color:{
    color: '#0d0d0d'
  }
});